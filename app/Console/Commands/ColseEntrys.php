<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\AttendanceEntry;

class ColseEntrys extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'close:entry';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close Attendance Entry.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        AttendanceEntry::where('status',0)->update(['status'=>1]);
    }
}
