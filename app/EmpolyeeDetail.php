<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpolyeeDetail extends Model
{
    public function user(){
        return $this->belongsTo(User::class);
    }
}
