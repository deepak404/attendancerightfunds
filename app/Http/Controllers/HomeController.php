<?php

namespace App\Http\Controllers;

use App\Holidays;
use App\Http\Middleware\User;
use Illuminate\Http\Request;
use App\EmpolyeeDetail;
use App\AttendanceEntry;
use Carbon\Carbon;
use DatePeriod;
use DateTime;
use DateInterval;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth');
     }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todayData = date('Y-m-d', strtotime(Carbon::now()));
        $data = $this->getData($todayData);


        return view('index',['data'=>$data]);
    }

    public function byDate(Request $request)
    {
        $date = date('Y-m-d',strtotime($request['date']));
        $data = $this->getData($date);
        return response()->json(['code'=>1,'data'=>$data, 'date' => $date]);
    }

    public function getData($date)
    {
        $data = AttendanceEntry::where('date',$date)->get()->toArray();
        $finalData = [];
        $count = 0;
        foreach ($data as $value) {
            $finalData[$count]['name'] = EmpolyeeDetail::where('empid',$value['empid'])->value('name');
            $finalData[$count]['date'] = $value['date'];
            $finalData[$count]['emp_id'] = $value['empid'];
            $finalData[$count]['intime'] = $value['intime'];
            $finalData[$count]['outtime'] = $value['outtime'];
            $count += 1;
        }
        return $finalData;
    }

    public function monthly($date = null)
    {
        if ($date == null) {
            $year = date('Y', strtotime(Carbon::now()));
            $month = date('m', strtotime(Carbon::now()));
        }else{
            $date = explode('-', $date);
            $year = $date[0];
            $month = $date[1];
        }
        $data = $this->getMonthData($month,$year);
        $firstday = $year.'-'.$month.'-01';
        $lastday = date('Y-m-t',strtotime($firstday. ' + 1 days'));
        $period = new DatePeriod(
             new DateTime($firstday),
             new DateInterval('P1D'),
             new DateTime($lastday)
        );
        $dateObj   = DateTime::createFromFormat('!m', $month);
        $monthName = $dateObj->format('F').' '.$year;
        return view('monthly',['data'=>$data,'date'=>$period,'month'=>$monthName]);
    }

    public function byMonth(Request $request)
    {
        $date = $request['date'];
        $date = explode('-', $date);
        $data = $this->getMonthData($date[1],$date[0]);
        $firstday = $date[0].'-'.$date[1].'-01';
        $lastday = date('Y-m-t',strtotime($firstday));
        $period = new DatePeriod(
             new DateTime($firstday),
             new DateInterval('P1D'),
             new DateTime($lastday)
        );
        return response()->json(['code'=>1,'data'=>$data]);
    }

    public function getMonthData($month,$year)
    {
        $employee = EmpolyeeDetail::all()->toArray();
        $finalData = [];
        $count = 0;
        foreach ($employee as $value) {
            // $finalData[$value['name']]=[];
            $data = AttendanceEntry::where('empid',$value['empid'])->where('date','LIKE',$year.'-'.$month.'%')->get();
            foreach ($data as $selected) {
                $finalData[$value['name']][$selected['date']]=['intime'=>$selected['intime'],'outtime'=>$selected['outtime']];
            }
            $count += 1;
        }
        // dd($finalData);  
        return $finalData;
    }

    public function registerEmp()
    {
        return view('register');
    }

    public function addEmp(Request $request)
    {
        $empid = EmpolyeeDetail::where('empid',$request['empId'])->get();
        if (count($empid) > 0) {
            return response()->json(['code'=>0,'msg'=>'Emp Id Exists Already']);
        }

        $saveUser = \App\User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'role' => 1,
        ]);


        if ($saveUser) {
            $insert = EmpolyeeDetail::insert([
                'user_id' => $saveUser->id,
                'name'=>$request['name'],
                'mobile'=>$request['number'],
                'empid'=>$request['empId']]);

            return response()->json(['code'=>1,'msg'=>'Successfully Added']);            
        }else{
            return response()->json(['code'=>0,'msg'=>'Error..']);
        }
    }

    public function countDays($year, $month, $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count;
    }


    public function getMonthlyData($empid, $date = null, Request $request){

        if(Auth::user()->role == 1){
            if($empid != Auth::user()->employeeDetails->empid){
                return redirect('employee-attendance/'.Auth::user()->employeeDetails->empid);

            }
        }

        if($date == null){
            $month = date('m', strtotime(Carbon::now()));
            $year = date('Y', strtotime(Carbon::now()));

        }else{
            $month = date('m', strtotime($date));
            $year = date('Y', strtotime($date));

        }



        $monthEnd = date('t-m-Y', strtotime('01-'.$month.'-'.$year));
        $monthStart = date('01-m-Y', strtotime('01-'.$month.'-'.$year));


        $workingDays = [];
        $data = AttendanceEntry::where('empid',$empid)->where('date','LIKE',$year.'-'.$month.'%')->get();
        $nationalHolidaysList = Holidays::where('date','LIKE',$year.'-'.$month.'%')->get();
        $nationalHolidays = $nationalHolidaysList->count();

        $nationalHolidaysMonth = [];

        foreach ($nationalHolidaysList as $nh){
            $nationalHolidaysMonth[$nh['date']] = $nh['name'];
        }


        $sundays = $this->getSundays($monthStart, $monthEnd);

        $totalWorkingDays = 0;
        $lateCount = 0;


        foreach ($data as $selected) {

            if($selected['outtime'] == null){
                $selected['outtime'] = '17:30:00';
            }

            $workingHours = round((strtotime($selected['outtime']) - strtotime($selected['intime']))/3600, 2);

            if($workingHours > 5){
                $totalWorkingDays += 1;
            }else{
                $totalWorkingDays += 0.5;
            }

            $status;
            if($selected['intime'] > '09:30:00'){
                $status = 'late';
                $lateCount++;
            }else{
                $status = 'Present';
            }
            $dayWorkingHours = round((strtotime($selected['outtime']) - strtotime($selected['intime']))/3600, 2);
            if($dayWorkingHours < 5) {
                $status = 'halfday';
            }
            $workingDays[$selected['date']]=[
                'intime'=>$selected['intime'],
                'outtime'=>$selected['outtime'],
                'working_hours' => $dayWorkingHours,
                'status' => $status,
            ];
        }


        $finalData = array_merge($sundays, $workingDays);



        $begin  = new DateTime($monthStart);
        $end    = new DateTime($monthEnd);
        $currentDate = date('Y-m-d');
        $leaveTaken = 0;

        while ($begin <= $end){

            if($begin->format("Y-m-d") < $currentDate){
                    if(array_key_exists($begin->format("Y-m-d"), $sundays)){
                        $finalData[$begin->format("Y-m-d")] = [
                            'intime'=> '-',
                            'outtime'=> '-',
                            'working_hours' => 0,
                            'status' => 'Sunday'
                        ];
                    }elseif(array_key_exists($begin->format("Y-m-d"), $workingDays)){
                        $finalData[$begin->format("Y-m-d")] = $workingDays[$begin->format("Y-m-d")];
                    }elseif(array_key_exists($begin->format("Y-m-d"), $nationalHolidaysMonth)){
                        $finalData[$begin->format("Y-m-d")] = [
                            'intime'=> '-',
                            'outtime'=> '-',
                            'working_hours' => 0,
                            'status' => 'holiday',
                        ];
                    } else{
                        $leaveTaken++;
                        $finalData[$begin->format("Y-m-d")] = [
                            'intime'=> '-',
                            'outtime'=> '-',
                            'working_hours' => 0,
                            'status' => 'Absent'
                        ];
                    }
            }else{

                if(array_key_exists($begin->format("Y-m-d"), $sundays)){
                    $finalData[$begin->format("Y-m-d")] = [
                        'intime'=> '-',
                        'outtime'=> '-',
                        'working_hours' => 0,
                        'status' => 'Sunday'
                    ];
                }elseif(array_key_exists($begin->format("Y-m-d"), $nationalHolidaysMonth)){
                    $finalData[$begin->format("Y-m-d")] = [
                        'intime'=> '-',
                        'outtime'=> '-',
                        'working_hours' => 0,
                        'status' => 'holiday',
                    ];
                }else{
                    $finalData[$begin->format("Y-m-d")] = [
                        'intime'=> '-',
                        'outtime'=> '-',
                        'working_hours' => 0,
                        'status' => '-'
                    ];
                }

            }

            $begin->modify('+1 day');
        }


        ksort($finalData);
//        dd($finalData);

        $employee = EmpolyeeDetail::where('empid', $empid)->first();

//        $totalWorkingDays = count($workingDays);

        if($totalWorkingDays > 0 ){
            $averageWorkingHours = round(array_sum(array_column($workingDays, 'working_hours'))/$totalWorkingDays, 2);

        }else{
            $averageWorkingHours = 0;

        }



        $totalDaysInMonth = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

//        foreach ($nationalHolidaysMonth as $nhDate => $nh){
//            if(date('Y-m-d') > $nhDate){
//                $totalWorkingDays += 1;
//            }
//        }
//        $totalWorkingDays += count($nationalHolidaysMonth);

        if($request->ajax()){
            return response()->json([ 'results' => $finalData,
                'employee' => $employee,
                'averageWorkingHours' => $averageWorkingHours,
                'totalWorkingDays' => $totalWorkingDays,
                'nhm' => $nationalHolidaysMonth,
                'totalDays' => ($totalDaysInMonth - count($sundays)),
                'lateDays' => $lateCount,
                'leave' => $leaveTaken
            ]);

        }
        return view('monthly-result',
            [
                'results' => $finalData,
                'employee' => $employee,
                'averageWorkingHours' => $averageWorkingHours,
                'totalWorkingDays' => $totalWorkingDays,
                'totalDays' => ($totalDaysInMonth - count($sundays)),
                'nhm' => $nationalHolidaysMonth,
                'lateDays' => $lateCount,
                'leave' => $leaveTaken

        ]);



    }


    public function getSundays($monthStart, $monthEnd){
        $begin  = new DateTime($monthStart);
        $end    = new DateTime($monthEnd);
        $sundays = [];
        while ($begin <= $end){
            if($begin->format("D") == "Sun")
            {
                $sundays[$begin->format("Y-m-d")] = [];
            }

            $begin->modify('+1 day');
        }


        return $sundays;
    }

    public function deleteEmployee($empId){
        $employee = EmpolyeeDetail::where('empid',$empId)->first();

        if(!is_null($employee)){

            $user = \App\User::where('id',$employee->user_id)->delete();
        }
        $attendance = AttendanceEntry::where('empid',$empId)->delete();
        $employee->delete();
        return redirect()->back();
    }

}
