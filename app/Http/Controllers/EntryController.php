<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmpolyeeDetail;
use App\AttendanceEntry;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class EntryController extends Controller
{
	public function entry($key)
    {
		\Log::info('MyKey : '.$key);
        if (Cache::pull('oldkey') == $key) {
            Cache::put('oldkey', $key, 5000);
            return '$RFID=0#';
        }
        Cache::put('oldkey', $key, 5000);
        $qs = str_replace('$', '', $key); // get rid of the $
        $qs = str_replace('*', '', $qs); // get rid of the *

        $submissions = explode(',', $qs); // split the subs


        $SID = ''; // store for sid
        $MID = ''; // store for mid
        $entry = 0;

        for ($i = 0; $i<count($submissions); $i++) {
            // try{
                $sections = explode('&', $submissions[$i]);
                if($i == 0) {
                    $SID = $sections[0];
                    $MID = $sections[1];
                    $RFID = $sections[2];
                    $DOT = $sections[3];
                    $dateTime = $this->timeFormater($DOT);
                    $exist = AttendanceEntry::where('status','0')->where('empid',$RFID)->get()->toArray();
                    if (count($exist) > 0) {
                        $entry = AttendanceEntry::where('id',$exist[0]['id'])->update(['outtime'=>$dateTime[1],'status'=>'1']);
                    }else{
                        $bydate = AttendanceEntry::where('date',$dateTime[0])->where('empid',$RFID)->get()->toArray();
                        if (count($bydate) > 0) {
                            $entry = AttendanceEntry::where('id',$bydate[0]['id'])->update(['outtime'=>$dateTime[1]]);
                        }else{
                            $entry = AttendanceEntry::insert(['empid'=>$RFID,'date'=>$dateTime[0],'intime'=>$dateTime[1],'status'=>'0']);
                        }
                    }
                } else {
                    $RFID = $sections[0];
                    $DOT = $sections[1];
                    $dateTime = $this->timeFormater($DOT);
                    $exist = AttendanceEntry::where('status','0')->where('empid',$RFID)->get()->toArray();
                    if (count($exist) > 0) {
                        $entry = AttendanceEntry::where('id',$exist[0]['id'])->update(['outtime'=>$dateTime[1],'status'=>'1']);
                    }else{
                        $bydate = AttendanceEntry::where('date',$dateTime[0])->where('empid',$RFID)->get()->toArray();
                        if (count($bydate) > 0) {
                            $entry = AttendanceEntry::where('id',$bydate[0]['id'])->update(['outtime'=>$dateTime[1]]);
                        }else{
                            $entry = AttendanceEntry::insert(['empid'=>$RFID,'date'=>$dateTime[0],'intime'=>$dateTime[1],'status'=>'0']);
                        }
                    }
                }
            // }catch(\Exception $e){
            //  return $e->getMessage();
            // }
        }
        if (count($entry) > 0) {

            return '$RFID=0#';
        }
    }

    public function timeFormater($data)
    {
        $string = $data;

        $day = substr($string, 0, 2);
        $month = substr($string, 2, 2);
        $year = substr($string, 4, 4);

        $hour = substr($string, 8, 2);
        $min = substr($string, 10, 2);
        $sec = substr($string, 12, 2);

        $dateTime = $year.'-'.$month.'-'.$day.' '.$hour.':'.$min.':'.$sec;

        $time = strtotime($dateTime);

        $newformat[] = date('Y-m-d',$time);  
        $newformat[] = date('H:i:s',$time);     

        return $newformat;
    }
}
