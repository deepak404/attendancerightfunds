$(document).ready(function(){ 
	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

	$('#reg-form').on('submit',function(e){
		e.preventDefault();
		var formData = $('#reg-form').serialize();
		 $.ajax({
            type: 'POST',
            url: '/add_emp',
            data: formData,
            success:function(data){
          		alert(data.msg);
              location.reload();
            },
            error:function(){ 
                
            }
        });
   	});
});