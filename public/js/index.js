$(document).ready(function(){ 
	$.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

	$('#daily-date').on('change',function(){
		var formData = 'date='+$(this).val();
		 $.ajax({
            type: 'POST',
            url: '/bydate',
            data: formData,
            success:function(data){
            	console.log(data);
            	$('#daily-tbody').empty();
            	$.each(data.data, function(key, value){
            	$('#daily-tbody').append('<tr>'+
			        '<td><a href="admin-employee-attendance/'+value.emp_id+'/'+value.date+'">'+value.name+'</a></td>'+
			        '<td>'+value.intime+'</td>'+
			        '<td>'+value.outtime+'</td>'+
			      '</tr>');

            	});
            },
            error:function(){ 
                
            }
        });
	});
});