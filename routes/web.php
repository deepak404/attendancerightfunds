<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('home');
    	return redirect('/home');
    // if (\Auth::user() == null) {
	   //  return redirect('/login');
    // }else{
    // 	return redirect('/home');
    // }
});

Auth::routes();

Route::get('/', function () {
    $user = Auth::user();
    if (!$user) {
        return redirect('/login');
    } elseif ($user->role == 1) {

        $empId = \Illuminate\Support\Facades\Auth::user()->employeeDetails->empid;

        return redirect('/employee-attendance/'.$empId.'/'.date('Y-m-d'));
    }else{
        return redirect('/home');
    }
});

Route::middleware(['admin','auth'])->group(function(){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('/bydate','HomeController@byDate');

    Route::post('/bymonth','HomeController@byMonth');

    Route::get('/monthly/{date?}', 'HomeController@monthly')->name('monthly');

    Route::get('/registerEmp', 'HomeController@registerEmp')->name('registerEmp');

    Route::post('/add_emp', 'HomeController@addEmp');

    Route::get('admin-employee-attendance/{empId}/{date?}', 'HomeController@getMonthlyData');

    Route::get('delete-employee/{empId}', 'HomeController@deleteEmployee');

});


Route::get('/entry/{key}', 'EntryController@entry');



Route::middleware(['user','auth'])->group(function(){

    Route::get('/employee-attendance/{empId}/{date?}', 'HomeController@getMonthlyData')->name('employee-home');

});

Route::get('/holidays', function(){
    return view('holidays');
});


Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');