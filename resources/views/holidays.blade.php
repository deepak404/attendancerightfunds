<html>
<head>
    <meta charset="UTF-8">
    <title>RF Attendance System</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script  src="https://code.jquery.com/jquery-3.3.1.js"  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="  crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="css/master.css">
    <link rel="stylesheet" href="css/index.css">
</head>
<body>
@extends( \Auth::user()->role == 0 ? 'layouts.navbar' : 'layouts.employee' )

@section('content')
    <div class="container master">
        <h3>Holidays</h3>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Name</th>
                <th>Date</th>
            </tr>
            </thead>
            <tbody id="daily-tbody">
                @foreach(\App\Holidays::all() as $holiday)
                    <tr>
                        <td>{{$holiday->name}}</td>
                        <td>{{date('d-m-Y', strtotime($holiday->date))}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
<script src="js/index.js"></script>
</body>
</html>