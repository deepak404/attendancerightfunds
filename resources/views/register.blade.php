<html>
<head>
	<meta charset="UTF-8">
	<title>RF Attendance System</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script  src="https://code.jquery.com/jquery-3.3.1.js"  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="  crossorigin="anonymous"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="css/master.css">
	<link rel="stylesheet" href="css/register.css">
</head>
<body>
@extends( \Auth::user()->role == 0 ? 'layouts.navbar' : 'layouts.employee' )
	 @section('content')
		 <div class="container master">
		 	<h3>Register New Empolyee</h3>
		 	<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<form action="#" method="POST" id="reg-form">
							<div class="form-group">
								<label for="name">Name:</label>
								<input type="text" class="form-control" id="name" name="name" required>
							</div>
							<div class="form-group">
								<label for="email">Email:</label>
								<input type="text" class="form-control" id="email" name="email" required>
							</div>
							<div class="form-group">
								<label for="password">Password:</label>
								<input type="text" class="form-control" id="password" name="password" required>
							</div>
							<div class="form-group">
								<label for="number">Phone Number:</label>
								<input type="number" class="form-control" id="number" name="number" required>
							</div>
							<div class="form-group">
								<label for="empId">Emp Id:</label>
								<input type="number" class="form-control" id="empId" name="empId" required>
							</div>
							<button type="submit" class="btn btn-default">Submit</button>
						</form>
						<p><strong>Note</strong> : Emp id is generated after registering the employee in the finger print machine. Please use the same ID.</p>
					</div>
					<div class="col-md-6">
						<table class="table-bordered table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Mobile</th>
									<th>Emp Id</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								@foreach(\App\EmpolyeeDetail::all() as $emp)
								<tr>
									<td><a href="admin-employee-attendance/{{$emp->empid}}/{{date('Y-m-d')}}">{{$emp->name}}</a></td>
									<td>{{$emp->mobile}}</td>
									<td>{{$emp->empid}}</td>
									<td><a href="/delete-employee/{{$emp->empid}}">Delete</a></td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<p><strong>Note</strong> : Deleting Employee will delete all their attendance entries and other details. Once Delete, information aren't recoverable. </p>
					</div>
				</div>
			</div>
	 @endsection
	 <script src="js/register.js"></script>
</body>
</html>