<html>
<head>
	<meta charset="UTF-8">
	<title>RF Attendance System</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script  src="https://code.jquery.com/jquery-3.3.1.js"  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="  crossorigin="anonymous"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="{{url('css/master.css')}}">
	<link rel="stylesheet" href="{{url('css/index.css')}}">
	<style>
		thead > tr {
			background-color: #80808073;
		    color: #292727;
		}
		tbody > tr > th {
			background-color: #80808073;
		    color: #292727;
		}
	</style>
</head>
<body>
	 @extends('layouts.navbar')	
	 @section('content')
		 <div class="container-fluid master">
		 	<h3>Monthly Report  ({{$month}})</h3>
		 	<input type="month" id="daily-date" placeholder="Month">
		 	<table class="table table-bordered">
		 		<thead>
					<tr>
						<th>#</th>
						@foreach($data as $key=>$value)
							<th colspan="2">{{$key}}</th>
						@endforeach
					</tr>
					<tr>
						<th></th>
						@foreach($data as $attendance)
							<th>In</th>
							<th>out</th>
						@endforeach
					</tr>
			    </thead>
			    <tbody id="monthly-tbody">

					@foreach($date as $value)
					<?php $selected = $value->format('Y-m-d'); ?>
					<tr>
						<th>{{$value->format('d-m-Y')}}</th>
						@foreach($data as $attendance)
						<?php if (array_key_exists($selected, $attendance)): ?>
							<td>{{$attendance[$selected]['intime']}}</td>
							<td>{{$attendance[$selected]['outtime']}}</td>
							<?php else: ?>
							<td></td>
							<td></td>
						<?php endif ?>
						@endforeach
					</tr>
					@endforeach
			    </tbody>
		 	</table>
		 </div>
	 @endsection
	 <script src="{{url('js/monthly.js?v=1.0')}}"></script>
</body>
</html>