<html>
<head>
    <meta charset="UTF-8">
    <title>RF Attendance System</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script  src="https://code.jquery.com/jquery-3.3.1.js"  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="  crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="css/master.css">
    <link rel="stylesheet" href="css/index.css">

    <style>
        .Sunday, .holiday{
            background-color: #ffffa5;
        }

        .Absent{
            background-color: #ff9696;
        }

        .Present{
            background-color: #a9ffa9;
        }
        .late{
            background-color: #ff9800;
        }
        .halfday{
            background-color: #ffc44f;
        }

        .master{
            margin-top: 3%;
        }

        .tr{
            margin-top: 4%;
        }

    </style>
</head>
<body>
@extends( \Auth::user()->role == 0 ? 'layouts.navbar' : 'layouts.employee' )

@section('content')
    <div class="container master">
        <h3>Monthly Report - {{$employee->name}}</h3>
        <input type="date" id="daily-date" placeholder="Date">
        <div class="row tr">
           <div class="col-md-12 col-sm-12">
               <div class="col-md-9 col-sm-9">
                   <table class="table table-bordered">
                       <thead>
                       <tr>
                           <th>Date</th>
                           <th>In Time</th>
                           <th>Out Time</th>
                           <th>Working Hours</th>
                           <th>Status</th>
                       </tr>
                       </thead>
                       <tbody id="daily-tbody">

                       <?php //dd($employee, $results); ?>
                       @foreach($results as $date =>  $value)

                           <tr class="{{$value['status']}}">
                                <td>{{date('d-m-Y', strtotime($date))}}</td>
                                <td>{{$value['intime']}}</td>
                                <td>{{$value['outtime']}}</td>
                                <td>{{$value['working_hours']}}</td>
                                <td>{{$value['status']}}</td>
                            </tr>

                       @endforeach
                       </tbody>
                   </table>
               </div>
               <div class="col-sm-3 col-md-3">
                   <table class="table table-bordered">

                       <tbody>
                       <tr>
                           <td>DPIM</td>
                           <td id="td">{{$totalDays}}</td>
                       </tr>
                       <tr>
                           <td>TWD</td>
                           @if($leave > 0)
                           <td id="twd">{{$totalWorkingDays + 1}}</td>
                           @else
                               <td id="twd">{{$totalWorkingDays}}</td>
                           @endif
                       </tr>
                       <tr>
                           <td>AWH</td>
                           <td id="awh">{{$averageWorkingHours}}</td>
                       </tr>
                       <tr>
                           <td>PAY</td>
                           <td id="pay"></td>
                       </tr>
                       <tr>
                           <td>NLP</td>
                           <td id="nlp">{{$lateDays}}</td>
                       </tr>
                       <tr>
                           @if($leave > 0)
                               <td>SL</td>
                               <td id="sl">1</td>
                           @else
                               <td>SL</td>
                               <td id="sl">0</td>
                           @endif
                       </tr>
                       </tbody>
                   </table>

                   <form action="#" id="pay-form">
                       <div class="form-group">
                           <label for="salary">Salary</label>
                           <input type="text" id="salary" class="form-control" required>
                       </div>

                       <div class="form-group">
                           <input type="submit" class="btn btn-primary" value="Calculate">
                       </div>
                       <p>NOTE : All sick Leaves not taken would be reimbursed in cash to the employees at the end of the year. One Sick Leave will be added to TWD if a leave is taken.</p>
                       <p><strong>DPIM</strong> - Days Present in a month</p>
                       <p><strong>TWD</strong> - Total Working Days</p>
                       <p><strong>AWH</strong> - Average Working Hours</p>
                       <p><strong>NLP</strong> - No of Late Present</p>
                       <p><strong>SL </strong>- Sick Leave</p>
                       <p><strong>Monthly Pay =</strong> (TWD/DPIM) * Salary</p>
                   </form>
               </div>
           </div>
        </div>
    </div>
@endsection
{{--<script src="js/index.js"></script>--}}

<script>
    $(document).ready(function(){
        var role = {!! json_encode(\Auth::user()['role']) !!};
        console.log(role);
        var empId = {!! json_encode($employee['empid']) !!}
            var uri = '/admin-employee-attendance/';
        if(role == 1){
            var uri = '/employee-attendance/';
        }

       $('#pay-form').on('submit', function(e){
           e.preventDefault();
           var twd = parseInt($('#twd').text());
           var dpim = parseInt($('#td').text());


           var salary = (twd /dpim ) * $('#salary').val();

           $('#pay').text(salary.toFixed(2));
       });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#daily-date').on('change',function(){
            // var formData = 'date='+$(this).val();
            var url = uri+empId+'/'+$(this).val();
            console.log(url);
            $.ajax({
                type: 'GET',
                url: url,
                data: '',
                success:function(data){
                    console.log(data);
                    $('#daily-tbody').empty();

                    $.each(data.results, function(k,v){
                        $('#daily-tbody').append(
                            '<tr class='+v.status+'>'+
                                '<td>'+k+'</td>'+
                                '<td>'+v.intime+'</td>'+
                                '<td>'+v.outtime+'</td>'+
                                '<td>'+v.working_hours+'</td>'+
                                '<td>'+v.status+'</td>'+
                            '</tr>'

                        )
                    });

                    $('#td').text(data.totalDays);
                    $('#awh').text(data.averageWorkingHours);
                    $('#nlp').text(data.lateDays);
                    if(data.leave > 0){
                        console.log(data.leave);
                        $('#sl').text(1);
                        $('#twd').text(data.totalWorkingDays + 1);

                    }else{
                        $('#twd').text(data.totalWorkingDays);

                    }


                },
                error:function(){

                }
            });
        });
    });
</script>
</body>
</html>