<html>
<head>
	<meta charset="UTF-8">
	<title>RF Attendance System</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<script  src="https://code.jquery.com/jquery-3.3.1.js"  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="  crossorigin="anonymous"></script>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<link rel="stylesheet" href="css/master.css">
	<link rel="stylesheet" href="css/index.css">
</head>
<body>

	 @extends( \Auth::user()->role == 0 ? 'layouts.navbar' : 'layouts.employee' )
	 {{--@extends('layouts.navbar')	--}}
	 @section('content')
		 <div class="container master">
		 	<h3>Daily Report</h3>
		 	<input type="date" id="daily-date" placeholder="Date">
		 	<table class="table table-bordered">
		 		<thead>
			      <tr>
			        <th>Name</th>
			        <th>In Time</th>
			        <th>Out Time</th>
			      </tr>
			    </thead>
			    <tbody id="daily-tbody">

					<?php $employee = \App\EmpolyeeDetail::all()->groupBy('empid')->toArray();	 ?>
			    	@foreach($data as $value)
							<?php unset($employee[$value['emp_id']]); ?>
							<tr>
								<td><a href="/admin-employee-attendance/{{$value['emp_id']}}">{{$value['name']}}</a></td>
								<td>{{$value['intime']}}</td>
								<td>{{$value['outtime']}}</td>
							</tr>
					@endforeach
					@foreach($employee as $empId => $value)

						<tr class="absent">
							<td><a href="/admin-employee-attendance/{{$empId}}">{{$value[0]['name']}}</a></td>
							<td>-</td>
							<td>-</td>
						</tr>
					@endforeach
			    </tbody>
		 	</table>
		 </div>
	 @endsection
	 <script src="js/index.js"></script>
</body>
</html>